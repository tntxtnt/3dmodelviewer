#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <GL/glu.h>
#include <GL/glut.h>
#include "TgaFile.h"

namespace obj3
{
    struct Vector2f { float x, y; };
    struct Vector3f { float x, y, z; };
    struct Vector3i { int x, y, z; };

    namespace complete
    {
        class SinglePartAndTextureOrder3
        {
        public:
            bool load(const char*, const char*);
            bool draw()const;
            const TgaFile& texImage()const { return texImg; }
        private:
            std::vector<float> fVertices;
            std::vector<float> fTexCoords;
            std::vector<float> fNormals;
            TgaFile texImg;
        public:
            GLfloat matDiffuse[4];
            GLfloat matSpecular[4];
            GLfloat matEmission[4];
            GLfloat matShininess[1];
        };
    }
}

#endif // OBJECT3D_H
