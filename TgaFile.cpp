#include "TgaFile.h"


bool TgaFile::load(const char* filename)
{
    FILE *filePtr;
    unsigned char ucharBad;
    short sintBad;
    long imageSize;
    int colorMode;
    unsigned char colorSwap;

    // Open the TGA file.
    filePtr = fopen(filename, "rb");
    if (!filePtr) return false;

    // Read the two first bytes we don't need.
    fread(&ucharBad, sizeof(unsigned char), 1, filePtr);
    fread(&ucharBad, sizeof(unsigned char), 1, filePtr);

    // Which type of image gets stored in typeCode.
    fread(&typeCode, sizeof(unsigned char), 1, filePtr);

    // For our purposes, the type code should be 2 (uncompressed RGB image)
    // or 3 (uncompressed black-and-white images).
    if (typeCode != 2 && typeCode != 3)
    {
        fclose(filePtr);
        return false;
    }

    // Read 13 bytes of data we don't need.
    fread(&sintBad, sizeof(short), 1, filePtr);
    fread(&sintBad, sizeof(short), 1, filePtr);
    fread(&ucharBad, sizeof(unsigned char), 1, filePtr);
    fread(&sintBad, sizeof(short), 1, filePtr);
    fread(&sintBad, sizeof(short), 1, filePtr);

    // Read the image's width and height.
    fread(&w, sizeof(short), 1, filePtr);
    fread(&h, sizeof(short), 1, filePtr);

    // Read the bit depth.
    fread(&bitCount, sizeof(unsigned char), 1, filePtr);

    // Read one byte of data we don't need.
    fread(&ucharBad, sizeof(unsigned char), 1, filePtr);

    // Color mode -> 3 = BGR, 4 = BGRA.
    colorMode = bitCount / 8;
    imageSize = w * h * colorMode;

    // Allocate memory for the image data.
    data = new unsigned char[imageSize];

    // Read the image data.
    fread(data, sizeof(unsigned char), imageSize, filePtr);

    // Change from BGR to RGB so OpenGL can read the image data.
    for (int imageIdx = 0; imageIdx < imageSize; imageIdx += colorMode)
    {
        colorSwap = data[imageIdx];
        data[imageIdx] = data[imageIdx + 2];
        data[imageIdx + 2] = colorSwap;
    }

    fclose(filePtr);
    return true;
}

