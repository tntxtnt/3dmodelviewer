#include <iostream>
#include <cstdlib> //exit
#include "Object3D.h"
#include <cmath> //atan,cos,sin

const GLfloat ZOOM_PCT = 0.05f; // zoom percentage
const GLfloat MIN_LOOK_DISTANCE = 0.2f;
const GLfloat ROT_ANGLE = atan(1.0) /  15; // 3 degree
const GLfloat COS_ROT_ANGLE = cos(ROT_ANGLE);
const GLfloat SIN_ROT_ANGLE = sin(ROT_ANGLE);

int windowWidth = 640, windowHeight = 480;
float windowRatio = (float)windowWidth / windowHeight;
GLfloat lookfrom[3], lookat[3], up[3], fovy, camera[3];
GLfloat lightPosition[4], lightColor[4], lightAmbient[4];
GLfloat lookDistance = 1.0f; //zooming helper
bool perspective = true;
GLfloat orthoRight = 10.0f; //half width of ortho view
obj3::complete::SinglePartAndTextureOrder3 object;

void zoom(GLfloat*, float&, float);
void rotateAroundYaxis(GLfloat*, bool);
void rotateAroundXaxis(GLfloat*, bool);
void loadModel();
void init(int&, char**);
void render();
void idle();
void resize(int, int);

int main(int argc, char** argv)
{
    loadModel();
	init(argc, argv);
	glutMainLoop();
}


void getLineWithoutComment(std::istream& is, std::istringstream& iss)
{
    std::string line;
    while (std::getline(is, line) && (line.empty() || line[0]=='#'));
    iss.clear();
    iss.str(line);
}

bool loadInputFile(const char* inputFilePath)
{
    std::ifstream fin(inputFilePath);
    if (!fin) return false;
    std::istringstream iss;
    getLineWithoutComment(fin, iss);
    iss >> windowWidth >> windowHeight;
    getLineWithoutComment(fin, iss);
    iss >> lookfrom[0] >> lookfrom[1] >> lookfrom[2];
    getLineWithoutComment(fin, iss);
    iss >> lookat[0] >> lookat[1] >> lookat[2];
    getLineWithoutComment(fin, iss);
    iss >> up[0] >> up[1] >> up[2];
    getLineWithoutComment(fin, iss);
    iss >> fovy;
    getLineWithoutComment(fin, iss);
    iss >> lightPosition[0] >> lightPosition[1]
        >> lightPosition[2] >> lightPosition[3];
    getLineWithoutComment(fin, iss);
    iss >> lightColor[0] >> lightColor[1]
        >> lightColor[2] >> lightColor[3];
    getLineWithoutComment(fin, iss);
    iss >> lightAmbient[0] >> lightAmbient[1]
        >> lightAmbient[2] >> lightAmbient[3];
    getLineWithoutComment(fin, iss);
    iss >> object.matDiffuse[0] >> object.matDiffuse[1]
        >> object.matDiffuse[2] >> object.matDiffuse[3];
    getLineWithoutComment(fin, iss);
    iss >> object.matSpecular[0] >> object.matSpecular[1]
        >> object.matSpecular[2] >> object.matSpecular[3];
    getLineWithoutComment(fin, iss);
    iss >> object.matEmission[0] >> object.matEmission[1]
        >> object.matEmission[2] >> object.matEmission[3];
    getLineWithoutComment(fin, iss);
    iss >> object.matShininess[0];
    fin.close();

    // Initialize camera's position to lookfrom's position
    for (int i = 0; i < 3; ++i) camera[i] = lookfrom[i];
    return true;
}

void loadModel()
{
    // Load config
    std::ifstream config("config.ini");
    if (!config)
    {
        std::cerr << "Cannot open config file\n";
        exit(1);
    }
    std::string line, objectName, modelPath;
    while (std::getline(config, line))
    {
        if (line.find("model") == 0)
            objectName = line.substr(line.find('=') + 1);
        if (line.find("path") == 0)
            modelPath = line.substr(line.find('=') + 1);
    }
    config.close();
    // Build paths from config
    std::string path = modelPath + objectName + "/" + objectName;
    std::string objectPath = path + ".obj";
    std::string texturePath = path + ".tga";
    std::string inputPath = path + ".inp";

    // Load object & texture image
    if (!object.load(objectPath.c_str(), texturePath.c_str()))
    {
        std::cerr << "Cannot load object file\n";
        exit(1);
    }

    // Load input file
    if (!loadInputFile(inputPath.c_str()))
    {
        std::cerr << "Cannot open input file\n";
        exit(1);
    }
}

void resize(int w, int h)
{
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0) h = 1;
	windowWidth = w;
	windowHeight = h;
    windowRatio = (float)windowWidth / windowHeight;
    // Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);
    // Reset Matrix
	glLoadIdentity();
	// Set the viewport to be the entire window
	glViewport(0, 0, windowWidth, windowHeight);
	if (perspective)
    {
        gluPerspective(fovy, windowRatio, 0.1f, 1000.0f);
    }
    else
    {
        float dist = 0.0f;
        for (int i = 0; i < 3; ++i)
            dist += pow(camera[i]-lookat[i], 2.0f);
        dist = sqrt(dist);
        orthoRight = dist * tan(fovy / 45 * atan(1.0f) / 2.0f);
        glOrtho(-orthoRight, orthoRight,
                -orthoRight/windowRatio, orthoRight/windowRatio,
                0.1f, 1000.0f);
    }
	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void render()
{
	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Reset transformations
	glLoadIdentity();
	// Set the camera
	gluLookAt(camera[0], camera[1], camera[2],
              lookat[0], lookat[1], lookat[2],
              up[0], up[1], up[2]);

    object.draw();

	glutSwapBuffers();
}

void idle()
{
    render();
    Sleep(15);
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 'r':
        for (int i = 0; i < 3; ++i)
            camera[i] = lookfrom[i];
        lookDistance = 1.0f;
        perspective = true;
        break;
    case 'c':
        perspective = !perspective;
        resize(windowWidth, windowHeight);
        break;
    case 'w':
        zoom(camera, lookDistance, -ZOOM_PCT);
        break;
    case 's':
        zoom(camera, lookDistance, ZOOM_PCT);
        break;
    case 'a':
        rotateAroundYaxis(camera, true);
        break;
    case 'd':
        rotateAroundYaxis(camera, false);
        break;
    case 'q':
        rotateAroundXaxis(camera, true);
        break;
    case 'e':
        rotateAroundXaxis(camera, false);
        break;
    default:
        break;
    }
}

void init(int& argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("Object3D - Test Driver");

    glShadeModel(GL_SMOOTH);

    // texture (single)
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, object.texImage().w,
                 object.texImage().h, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 (GLvoid*)object.texImage().data);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);

    // material
    glMaterialfv(GL_FRONT, GL_DIFFUSE, object.matDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, object.matSpecular);
    glMaterialfv(GL_FRONT, GL_EMISSION, object.matEmission);
    glMaterialfv(GL_FRONT, GL_SHININESS, object.matShininess);
    glEnable(GL_COLOR_MATERIAL);

    // lighting
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);

    // register functions
	glutDisplayFunc(render);
	glutReshapeFunc(resize);
	glutIdleFunc(idle);
	glutKeyboardFunc(keyboard);
}

void zoom(GLfloat* cam, float& lookDist, float distance)
{
    float newLookDist = lookDist + distance;
    if (newLookDist < MIN_LOOK_DISTANCE)
        newLookDist = MIN_LOOK_DISTANCE;
    for (int i = 0; i < 3; ++i)
        cam[i] = newLookDist / lookDist * (cam[i] - lookat[i]) + lookat[i];
    lookDist = newLookDist;
    if (!perspective) resize(windowWidth, windowHeight);
}

void rotateAroundYaxis(GLfloat* cam, bool reversed)
{
    float a, b;
    cam[0] -= lookat[0];
    cam[2] -= lookat[2];
    if (!reversed)
    {
        a = cam[0] * COS_ROT_ANGLE + cam[2] * SIN_ROT_ANGLE;
        b = cam[2] * COS_ROT_ANGLE - cam[0] * SIN_ROT_ANGLE;
    }
    else
    {
        a = cam[0] * COS_ROT_ANGLE - cam[2] * SIN_ROT_ANGLE;
        b = cam[2] * COS_ROT_ANGLE + cam[0] * SIN_ROT_ANGLE;
    }
    cam[0] = a + lookat[0];
    cam[2] = b + lookat[2];
}

void rotateAroundXaxis(GLfloat* cam, bool reversed)
{
    float a, b;
    cam[1] -= lookat[1];
    cam[2] -= lookat[2];
    if (!reversed)
    {
        a = cam[2] * COS_ROT_ANGLE + cam[1] * SIN_ROT_ANGLE;
        b = cam[1] * COS_ROT_ANGLE - cam[2] * SIN_ROT_ANGLE;
    }
    else
    {
        a = cam[2] * COS_ROT_ANGLE - cam[1] * SIN_ROT_ANGLE;
        b = cam[1] * COS_ROT_ANGLE + cam[2] * SIN_ROT_ANGLE;
    }
    cam[2] = a + lookat[2];
    cam[1] = b + lookat[1];
}
